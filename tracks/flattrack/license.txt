Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files: *.blend
Source: https://sourceforge.net/p/supertuxkart/code/1400/tree/trunk/media/tracks/race/flattrack8.blend
Copyright: originally by (??) from original (Super)TuxKart teams, modifications 2017, 2019 Richard Qian
License: GPL-3+

Files: tk2.ogg
Copyright: Matt Thomas (from STK 0.3)
License: GPL-2+
