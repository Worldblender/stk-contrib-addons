# Track: Mobius Strip
![Screenshot of this track](sshot-mobius-strip.png)

*My first released track using the new STK exporter scripts which can be found at <https://github.com/RQWorldblender/stk-blender> or its mirror <https://gitlab.com/Worldblender/stk-blender>.*

## Origin
A track based on the eponymous mathematical surface. More information can be found at <https://en.wikipedia.org/wiki/M%C3%B6bius_strip>. The original model is based on content from https://www.thingiverse.com/thing:239158, originally submitted by kitwallace. Obtained via Wikimedia Commons at https://commons.wikimedia.org/wiki/File:M%C3%B6bius_strip_(with_thickness).stl. 

## Appearance
The start line, checklines (marked by the checkered poles), and the skybox imply that this track takes place in space, but on the moon. The overall appearance of the track gives it the impression that it is a spacecraft, further adding to the space theme. The music, while a chiptune one, gives the feeling of being in outer space.

## Driving
There will be driving sideways and upside-down; this may look simple to drive, but it's not really (it's not an actual ring!). It's not possible to drive this track without turning, but both sides of the track will be traversed by all karts. AI karts on this track tend to drive into bananas more often than on other tracks, likely because are almost no relatively flat surfaces here. The minimap flattens the whole track, so it's quite easy for players to get confused as to where their kart is at, especially if other karts are in front of or behind them.

## Errata
When racing on this track in reverse mode, the rank counter sometimes behaves erratically. This is more noticeable when racing with more than two karts, as some of these karts's ranks may jump to other positions randomly. It doesn't cause problems when racing in normal or time trial modes, but it may cause problems in follow the leader mode.
