Based on content from https://www.thingiverse.com/thing:239158, originally submitted by kitwallace. Obtained via Wikimedia Commons at https://commons.wikimedia.org/wiki/File:M%C3%B6bius_strip_(with_thickness).stl. Original file licensed under CC-BY-SA 4.0 International License.

Converted into a track by Richard Qian (Worldblender) and released under CC-BY-SA 4.0 International License (main license), excluding built-in STK textures and the following textures:

* rainbow3.png taken from track Nostalgia by Anon, which in turn is taken from older SuperTuxKart versions (0.5 and older). Licensed under the GNU GPL v2+.

Files: indigo_??.jpg
Source: https://opengameart.org/content/spacemoon-skybox
Copyright: skiingpenguins / freezurbern
License: CC-BY-SA 3.0

Files: back_to_the_game.ogg
Source: https://opengameart.org/content/back-to-the-game
Copyright: Mega Pixel Music Lab
License: CC-BY 4.0
