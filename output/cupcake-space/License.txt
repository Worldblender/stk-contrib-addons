Released under CC-BY-SA 3.0

Planet models and textures (only for the moon, Mercury, Saturn rings, Neptune, and Pluto) appended from former official track Star Track.
The cupcake model and texture are the same as the one used for the powerup of the same name.

Files: back.png, blue_*.png
Source: https://opengameart.org/content/space-skyboxes, blue0 collection
Copyright: arikel
License: CC0
