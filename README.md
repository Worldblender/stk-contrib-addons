# Worldblender's Contributed Content for SuperTuxKart
My personal collection of content for the free/open source (FOSS) racing game 
[SuperTuxKart](https://supertuxkart.net/). They are a mix of former official content, add-on content, and maybe in the future original content by me. The bulk of the content found here consists of tracks, although there's a few arenas and karts found here currently. Some of this content will also be uploaded to the [Official STK addon website](https://online.supertuxkart.net/) in the future.

## How to use/install
### If just installing
Copy or symlink the contents of the 'output/' directory to one of these paths, depending on your operating system:

* On Windows: %APPDATA%/supertuxkart/addons/karts

* On Linux distros/BSDs: ~/.local/share/supertuxkart/addons/karts

* On macOS: ~/Library/Application Support/supertuxkart/addons/karts

* On Android: <location of internal storage>/stk/home/.local/share/supertuxkart/addons/karts

### If developing the tracks
Copy or symlink the contents of the 'tracks/' and 'music/' directories to the location of the STK media repository (instructions on how to download it are at <https://supertuxkart.net/Media_Repo>. Alternatively, it is possible to just symlink the 'library/' directory if it is desired to keep this collection self-contained. Textures from STK itself may be present and used by tracks, but they are not copied over to the output.

## Source of tracks, if not created by me
* TuxFamily is still hosting the old stkaddons files: <http://download.tuxfamily.org/stkaddons/>
* Google Drive mirror: <https://drive.google.com/open?id=10VpNHsk-_FjVD1bJXDH0OuMs9N0QeWu7>
* Some tracks can be downloaded from the current STK addons website.
* The B3D importer at <https://github.com/joric/io_scene_b3d> is useful for dealing with karts and tracks whose original author(s) never published the original Blender files for them. For STK content published for versions from 0.7 to 0.9.2.
* The AC3D scripts at <https://github.com/majic79/Blender-AC3D> are useful for dealing with karts and tracks whose original author(s) never published the original Blender files for them. For STK content published for versions 0.6.2 and older.

## Licensing
Unless other specified, all tracks are licensed under either GPL-3.0+ or CC-BY-SA 4.0+. Check the license file in each track directory for more details.
