default_apple.obj, default_apple_3d.png: created by VanessaE, licensed under CC BY-SA 4.0+.

Library nodes below are created by rubberduck, and are licensed under CC-BY-SA 4.0+ (they are from the Canyon42 track found at https://forum.freegamedev.net/viewtopic.php?f=90&t=11348, and the Antediluvian Palace arena found at https://forum.freegamedev.net/viewtopic.php?f=18&t=11312):

Canyon42:

stklib_container_a
    model from me, texture from BMacZero (CC0)
    see https://opengameart.org/content/us-navy-seabee-equipment-container
    and https://opengameart.org/content/container-0

stklib_barrel_a
    model and texture from from https://opengameart.org/content/barreloil02 (CC0, by yughues)
    barrel.png modified (added hazard warning symbol from openclipart: https://openclipart.org/detail/14430/biological-hazard-warning-sign, CC0)

stklib_military_vehicle_a
    model and texture based on
    https://opengameart.org/content/willys-jeep-asset (CC-BY-SA 3.0, by Johannes Nienaber Videogame Development)

stklib_ssaoVan_a
    model from stklib_tvVan_a
    texture from GeekPenguinBR, see https://forum.freegamedev.net/viewtopic.php?f=18&t=8214&start=25#p79657 (BSD / GPL license)
    normal map from MR.XX99, see https://forum.freegamedev.net/viewtopic.php?f=18&t=8214&start=25#p81436

stklib_cactus_a
    model and texture from https://opengameart.org/content/freebies-mundo-commissions (CC0, by yughues)

stklib_concrete_barrier_a (model and textures)
    from https://opengameart.org/content/concrete-barriers (CC0, by yughues)

Antediluvian Palace:

stklib_crystal_a
    model and textures based on mines track (textures baked from emerald.png and chrome.png)

stklib_puffyStatue_a
    based on puffy kart
    original model and texture from MiniBjorn, CC-BY-SA 3.0
