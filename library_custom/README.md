# A list of sources for the contents of the library_custom directory
Unless otherwise stated, all assets listed below are under the same license as their original sources.

* default_apple.obj, default_apple_3d.png: https://gitlab.com/VanessaE/dreambuilder_modpack/blob/master/dreambuilder_mp_extras/models/default_apple.obj, https://gitlab.com/VanessaE/dreambuilder_modpack/blob/master/dreambuilder_mp_extras/textures/default_apple_3d.png
* Racetrack/ : https://opengameart.org/content/modular-racetrack-3d-models
* lowpoly_building/ : https://opengameart.org/content/low-poly-skyscraper
* dist/ : https://opengameart.org/content/voxel-buildings
* skyboxes/ : https://opengameart.org/content/space-skyboxes
* skateboard/ : https://opengameart.org/content/skateboard
* steering-wheel/ : https://opengameart.org/content/steering-wheel
* traffic-cone/ : https://opengameart.org/content/traffic-cone
* Voxel Vehicle Starter Kit/ : https://opengameart.org/content/voxel-vehicle-starter-pack

* PacmanFinal.blend: https://opengameart.org/content/marbleman-gobbler-with-animation, modified to include an armature and different animations
* ske.blend: https://opengameart.org/content/ske-supertux3d
* stilagmate.blend: https://opengameart.org/content/stilagmate-supertux3d
* StreetSigns.blend: https://opengameart.org/content/basic-street-signs
* HousesPack/HousesPack.blend: Derived from https://opengameart.org/content/houses-pack-low-poly-v02, modified to include textures
* vroum-car.zip: https://opengameart.org/content/vroum-car

## Usage
Append to any track, or link if a custom STK library object, as these files are not shipped with SuperTuxKart. If linking, they should be set up as a proxy.
