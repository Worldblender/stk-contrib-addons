# Kart: Minetest Sam
![Screenshot of this kart](sam-screenshot.png)

*My first released kart using the new STK exporter scripts which can be found at <https://github.com/RQWorldblender/stk-blender> or its mirror <https://gitlab.com/Worldblender/stk-blender>.*

## Basic facts
* Kart type: Medium
* Engine sounds: Large
* Color (web/hex): #e6e600

## Origin
A kart based on the default character skin shipping with Minetest Game. From <https://wiki.minetest.net/Player>:

> The official name given for the player's appearance is “Sam”, which is a recursive acronym for “Sam ain’t Minecraft”.

Minetest Game has vehicles, but only boats and minecarts are found in the base game. Instead, the car used is from https://opengameart.org/content/vroum-car, made by Gundy and licensed under CC0.

## Appearance
The model used comes directly from Minetest Game, with minor edits (removed double vertices). The same skin is used, but it has been upscaled in order to better preserve the character's blocky appearance. Almost every texture used, with the exceptions of the wheels and headlights, originate from the base Minetest distribution and Minetest Game.

Most of the vehicle has been remeshed to become voxelized. A few objects have not been voxelized in order to keep the kart design from becoming too complex. 

These include:

* Wheels
* Seat
* Steering Wheel
* Headlights
* Exhaust pipes
* License plate

A reading is written in blocks on the hood of the car: M T 1 -> Minetest is #1 | S T K -> SuperTuxKart

## References to upstream source
* The kart color is #e6e600, one the primary colors of mese, an original material (made for this game).
* The whole kart is made almost of metals, not stone or similar materials, as stone cannot be put together like this in a real kart.
* The furnace at the front of the vehicle acts like a grate, though it has no fuel. When they have fuel, they can emit a fire as an animation.
* The seat has red, white, and black coloring, similar to how beds in Minetest Game can only have red blankets (but that can change in the future).
* Mese stuffed in all four headlights give off light, just like in their block form. Each block alone emits considerably less light, however.
* Sam eats an apple and then loaf of bread in his winning animation. Apples can be found naturally in some trees, while bread has to be made from flour which in turn is made from wheat.
* Sam wields a pickaxe and shovel in his kart selection animation. An axe and sword are also found on the car. All four of these tools are made of diamond, the strongest material useful for crafting tools in Minetest Game.
